//
//  ViewController.m
//  APP Logo自动生成
//
//  Created by mac on 2017/8/11.
//  Copyright © 2017年 qiyun. All rights reserved.
//

#import "ViewController.h"
#import "CutLogoViewController.h"

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

#pragma 点击事件
- (IBAction)clickBtn:(id)sender {
    NSLog(@"%@",@"我被点击了");
    
    NSOpenPanel*  panel = [NSOpenPanel openPanel];
    [panel setAllowsOtherFileTypes:YES];
    [panel setAllowedFileTypes:@[@"png"]];
    
    [panel beginSheetModalForWindow:self.view.window completionHandler:^(NSInteger result){
      if (result == NSFileHandlingPanelOKButton)
           {
               NSString *path = [[panel URL] path];
               NSLog(@"%@", path);
               
               NSStoryboard *sb=[NSStoryboard storyboardWithName:@"Main" bundle:nil];
               CutLogoViewController *cutVc=(CutLogoViewController *)[sb instantiateControllerWithIdentifier:@"cutVc"];
               cutVc.imgUrl=path;
               [self.view.window.contentViewController presentViewControllerAsModalWindow:cutVc];
        }
    }];
}


- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

}
@end
