//
//  CutLogoViewController.h
//  APP Logo自动生成
//
//  Created by mac on 2017/8/11.
//  Copyright © 2017年 qiyun. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface CutLogoViewController : NSViewController
@property (weak) IBOutlet NSImageView *logoImgView;
@property (weak) IBOutlet NSButton *cutAndroidBtn;
@property (weak) IBOutlet NSButton *cutIosBtn;
@property (weak) IBOutlet NSButton *cutAllBtn;
@property (nonatomic,copy) NSString *imgUrl;
@end
