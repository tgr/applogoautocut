//
//  CutLogoViewController.m
//  APP Logo自动生成
//
//  Created by mac on 2017/8/11.
//  Copyright © 2017年 qiyun. All rights reserved.
//

#import "CutLogoViewController.h"

@interface CutLogoViewController ()

@end

@implementation CutLogoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(_imgUrl!=nil){
        _logoImgView.image= [[NSImage alloc]initWithContentsOfFile:_imgUrl];
    }

}


- (IBAction)clickBtn:(id)sender {
    NSSavePanel*  panel = [NSSavePanel savePanel];
    [panel setCanCreateDirectories:YES];
    [panel setNameFieldStringValue:@"cutlogo"];
    
    [panel beginSheetModalForWindow:self.view.window completionHandler:^(NSInteger result){
        if (result == NSFileHandlingPanelOKButton)
        {
            NSString *path = [[panel URL] path];
            NSLog(@"%@", path);
            if(path!=nil){
                if(sender==_cutIosBtn){
                    [self cutIos:path];
                }else if(sender==_cutAndroidBtn){
                    [self cutAndroid:path];
                }else{
                    [self cutAndroid:path];
                    [self cutIos:path];
                }
                NSLog(@"%@",sender);
            }
            
        }
    }];
}


#pragma 裁剪安卓logo并保存
-(void)cutAndroid:(NSString *)rootDir{
    NSArray *sizeArr=@[@192,@144,@96,@72,@48];
    NSArray *dirArr=@[@"mipmap-xxxhdpi",@"mipmap-xxhdpi",@"mipmap-xhdpi",@"mipmap-hdpi",@"mipmap-mdpi"];
    //首先创建android目录
    if(rootDir==nil){
        rootDir=@"/Users/mac/Downloads";
    }
    rootDir=[rootDir stringByAppendingPathComponent:@"AndroidLogo"];
    [self createDir:rootDir];
    //构建原始image
    NSImage *image=[[NSImage alloc]initWithContentsOfFile:_imgUrl];
    for(int i=0;i<sizeArr.count;i++){
        NSSize size=NSMakeSize([sizeArr[i] floatValue], [sizeArr[i] floatValue]);
        NSString *subDir=[rootDir stringByAppendingPathComponent:dirArr[i]];
        NSString *fileName=[subDir stringByAppendingPathComponent:@"ic_launcher.png"];
        //创建文件夹
        [self createDir:subDir];
        //改变大小并保存
        NSImage *newimage=[self scaleImage:image toSize:size];
        NSData *imageData = [newimage TIFFRepresentation];
        NSBitmapImageRep *imageRep = [NSBitmapImageRep imageRepWithData:imageData];
        NSData *imageData1 = [imageRep representationUsingType:NSPNGFileType properties:nil];
        [imageData1 writeToFile:fileName atomically:YES];
    }
}


#pragma 裁剪IOS logo并保存
-(void)cutIos:(NSString *)rootDir{
    NSArray *sizeArr=@[@180,@120];
    NSArray *fileNameArr=@[@"@3x",@"@2x"];
    //首先创建IOS目录
    if(rootDir==nil){
        rootDir=@"/Users/mac/Downloads";
    }
    rootDir=[rootDir stringByAppendingPathComponent:@"IOSLogo"];
    [self createDir:rootDir];
    //构建原始image
    NSImage *image=[[NSImage alloc]initWithContentsOfFile:_imgUrl];
    for(int i=0;i<sizeArr.count;i++){
        NSSize size=NSMakeSize([sizeArr[i] floatValue], [sizeArr[i] floatValue]);
        NSString *fileName=[rootDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@%@",@"launcher",fileNameArr[i],@".png"]];
        //改变大小并保存
        NSImage *newimage=[self scaleImage:image toSize:size];
        NSData *imageData = [newimage TIFFRepresentation];
        NSBitmapImageRep *imageRep = [NSBitmapImageRep imageRepWithData:imageData];
        NSData *imageData1 = [imageRep representationUsingType:NSPNGFileType properties:nil];
        [imageData1 writeToFile:fileName atomically:YES];
    }

}

#pragma 创建文件夹
-(BOOL)createDir:(NSString *)path{
    NSError *error;
    NSFileManager *manager = [NSFileManager defaultManager];
    BOOL a = [manager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:&error];
    return a;
}


#pragma 裁剪图片
-(NSImage *)scaleImage:(NSImage *)image toSize:(NSSize)targetSize{
    if ([image isValid]){
        NSSize imageSize = [image size];
        float width  = imageSize.width;
        float height = imageSize.height;
        float targetWidth  = targetSize.width;
        float targetHeight = targetSize.height;
        float scaleFactor  = 0.0;
        float scaledWidth  = targetWidth;
        float scaledHeight = targetHeight;
        
        NSPoint thumbnailPoint = NSZeroPoint;
        
        if (!NSEqualSizes(imageSize, targetSize))
        {
            float widthFactor  = targetWidth / width;
            float heightFactor = targetHeight / height;
            
            if (widthFactor < heightFactor)
            {
                scaleFactor = widthFactor;
            }
            else
            {
                scaleFactor = heightFactor;
            }
            
            scaledWidth  = width  * scaleFactor;
            scaledHeight = height * scaleFactor;
            
            if (widthFactor < heightFactor)
            {
                thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
            }
            
            else if (widthFactor > heightFactor)
            {
                thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
            }
            
            NSImage *newImage = [[NSImage alloc] initWithSize:targetSize];
            
            [newImage lockFocus];
            
            NSRect thumbnailRect;
            thumbnailRect.origin = thumbnailPoint;
            thumbnailRect.size.width = scaledWidth;
            thumbnailRect.size.height = scaledHeight;
            
            [image drawInRect:thumbnailRect
                     fromRect:NSZeroRect
                    operation:NSCompositingOperationSourceOver
                     fraction:1.0];
            
            [newImage unlockFocus];
            return newImage;
        }else{
            return image;
        }
    }else{
        return nil;
    }
}

@end
